\SweaveOpts{fig.path='figure/grace_simulation_', echo = TRUE, message = FALSE, warning = FALSE, fig.width=6, fig.height=3, out.width='.6\\linewidth', fig.align = 'center', tidy = FALSE, comment = NA, cache = FALSE, cache.path='cache/grace_simulation_'}

\documentclass{article}
\usepackage{mathpazo}
\renewcommand{\sfdefault}{lmss}
\renewcommand{\ttdefault}{lmtt}
\usepackage[T1]{fontenc}
\usepackage{geometry}
\geometry{verbose,tmargin=2.5cm,bmargin=2.5cm,lmargin=2.5cm,rmargin=2.5cm}
\usepackage{url}
\usepackage{animate} 
\usepackage[authoryear]{natbib}

\newcommand{\y}{Y_{ij}}
\newcommand{\n}{n_{i}}
\newcommand{\si}{\sum_{i=1}^{k}}
\newcommand{\xij}{X_{ij}^{T}}
\newcommand{\lem}{\left \{}
\newcommand{\rem}{\right \}}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.

\makeatother

\begin{document}

\title{Simulated grace}

\author{Michael C. Donohue}

\maketitle

\section{Simulated Data}

<<library>>= 
options(stringsAsFactors = FALSE)
library(grace)
library(ggplot2)
library(mvtnorm)
@
% setwd("~/rlib/grace_inst/simulations")

<<DataPrep>>=
set.seed(10)
x <- seq(-6, 6, by = 0.1)
f1 <- function(t){ 1 / (1 + exp(-t)) }
f2 <- function(t){ t / 12 + 0.5 }
f3 <- function(t){ (t+6)^2/144/2 }

pdd <- rbind(
  data.frame(x = x, y = f1(x), curve = "f1(x)"),
  data.frame(x = x, y = f2(x), curve = "f2(x)"),
  data.frame(x = x, y = f3(x), curve = "f3(x)"))  

qplot(x, y, data = pdd, colour = curve, linetype = curve, geom = "line")

t <- seq(-1, 1, by = 0.5)
n <- 100
id <- 1:n
gamma0 <- runif(n, min=-5, max=5)

dd <- cbind(id, gamma0)
dd <- merge(dd, data.frame(t))
sig <- 0.01
rho <- 0.005
dd <- data.frame(rbind(
  cbind(dd, Outcome = 1, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2))),
  cbind(dd, Outcome = 2, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2))),
  cbind(dd, Outcome = 3, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2)))))
colnames(dd) <- c("id", "gamma0", "t", "Outcome", "alpha0", "alpha1")
dd$e <- rnorm(nrow(dd), sd = 0.1)

dd$Y0 <- with(dd, 
  ifelse(Outcome == 1, f1(t + gamma0),
  ifelse(Outcome == 2, f2(t + gamma0),
  f3(t + gamma0))))
dd$Y <- with(dd, Y0 + alpha0 + alpha1 * t + e)
dd <- dd[with(dd, order(id, Outcome, t)), ]
dd$Outcome <- paste("Outcome", dd$Outcome)
@

<<trueshifts>>=
ggplot(dd, aes(t + gamma0, Y, group=id)) +
  geom_line(alpha = 1/8) +
  geom_line(aes(t + gamma0, Y0, group=NULL), size = 1.5) +
  facet_wrap(~ Outcome) +
  xlab(expression(t + gamma[0])) +
  xlim(-5, 5) +
  opts(title = "A. Simulated long-term data")
@

<<observed>>=
ggplot(dd, aes(t, Y, group=id)) +
  geom_line(alpha = 1/8) +
  geom_point(size = 1, alpha = 1/8) +
  facet_wrap(~ Outcome) +
  xlim(-5, 5) +
  opts(title = "B. Simulated short-term observations")
@

<<part_residuals_g>>=
ggplot(dd, aes(t+gamma0, Y - alpha0 - alpha1*t, group=id)) +
  geom_line(alpha = 1/8) +
  geom_point(size = 1, alpha = 1/8) +
  facet_wrap(~ Outcome) +
  xlim(-5, 5) +
  xlab(expression(t+gamma[0])) +
  ylab(expression(R^g * (t)*"="*Y[ij](t) - alpha[0*ij] - alpha[1*ij]*t)) +
  opts(title = "Partial residuals for estimating g")
@

<<part_residuals_alpha>>=
ggplot(dd, aes(t, Y - Y0, group=id)) +
  geom_line(alpha = 1/8) +
  geom_point(size = 1, alpha = 1/8) +
  facet_wrap(~ Outcome) +
  xlim(-5, 5) +
  xlab(expression(t)) +
  ylab(expression(R^alpha * (t)*"="*Y[ij](t) - g(t+gamma[i]))) +
  opts(title = expression("Partial residuals for estimating "*alpha))
@


<<gracefits, eval = TRUE, fig.width=7, fig.height=7>>=

# argvals <- dd$t; y <- dd$Y; outcome <- dd$Outcome; id <- dd$id
# group = NULL;
# tolerance = 1e-3; maxiter = 20;
# norder = 6; ngrid = 5; Lfdobj = 3; lambda = 10^(4);
# verbose = FALSE; plots = TRUE; figdir = file.path(getwd(), "iterationfigs")

grace.simulation.fits <- with(dd, grace(t, Y, Outcome, id, plots = TRUE))

save(grace.simulation.fits, file = "data/grace_simulation_fits.rdata")
@

<<sigma>>=
load("data/grace_simulation_fits.rdata")
ggplot(grace.simulation.fits$sigma, aes(Iteration, sigma, group=outcome))+
  geom_line(aes(colour = outcome)) +
  ylab(expression(sigma[epsilon]))
@

\newpage
\section{Results}
<<simallobserved>>=
load(file = "data/grace_simulation_fits.rdata")
dd1 <- do.call(rbind, lapply(grace.simulation.fits$fits, function(x) x$subs))
dd1$Outcome <-  factor(dd1$outcome, levels = paste("Outcome", 1:3))
dd1.true <- dd1
dd1.true$ghat <- with(dd1, 
  ifelse(Outcome == "Outcome 1", f1(argvals + gamma0),
  ifelse(Outcome == "Outcome 2", f2(argvals + gamma0),
  f3(argvals + gamma0))))
dd1.true$y <- NA
dd1$Curve <- "Estimated"
dd1.true$Curve <- "Target"
dd2 <- rbind(dd1, dd1.true)
dd2 <- dd2[!duplicated(with(dd2, paste(argvals + gamma0, Curve, Outcome))), ]
dd2 <- dd2[with(dd2, order(Curve, Outcome, argvals+gamma0)), ]
# dd2$Curve <- factor(dd2$Curve, levels = c("Target", "Estimated"))
ggplot(dd1, aes(argvals + gamma0, y, group=id))+
  geom_line(alpha = 1/8) +
  geom_line(aes(argvals + gamma0, ghat, group = Curve, colour = Curve, linetype = Curve), 
    data = dd2, size = 1.5) +
  xlim(-5, 5) +
  facet_wrap(~ Outcome) +
  opts(legend.position=c(0.775, 0.75)) +
  ylab("Y") + xlab(expression(t + hat(gamma))) +
  opts(title = "C. Long-term curves estimated from short-term observations")
@

<<gracesim1000fits, eval = FALSE, fig.width=7, fig.height=7>>=
set.seed(10)
for(S in 1:1000){
  gamma0 <- runif(n, min=-5, max=5)
  dd <- data.frame(id, gamma0)
  dd <- merge(dd, data.frame(t))
  dd <- data.frame(rbind(
    cbind(dd, Outcome = 1, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2))),
    cbind(dd, Outcome = 2, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2))),
    cbind(dd, Outcome = 3, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2)))))
  colnames(dd) <- c("id", "gamma0", "t", "Outcome", "alpha0", "alpha1")
  dd$e <- rnorm(nrow(dd), sd = 0.1)
  dd$Y0 <- with(dd, 
    ifelse(Outcome == 1, f1(t + gamma0),
    ifelse(Outcome == 2, f2(t + gamma0),
    f3(t + gamma0))))
  dd$Y <- with(dd, Y0 + alpha0 + alpha1 * t + e)
  dd <- dd[with(dd, order(id, Outcome, t)), ]
  dd$Outcome <- paste("Outcome", dd$Outcome)

  grace.S.fits <- with(dd, grace(t, Y, Outcome, id, plots = FALSE))
  save(grace.S.fits, file = paste("simresults/grace", S, "fits.rdata", sep = "_"))
}
psdata <- c()
if(FALSE){
  for(S in 1:1000){
    load(file = paste("simresults/grace", S, "fits.rdata", sep = "_"))
    psdata <- rbind(psdata, cbind(S = S, predict(grace.S.fits, new.argvals.gammas = seq(-10, 10, by = 0.5))))
  }
  save(psdata, file = "simresults/grace_predictions.rdata")
}
@

<<gracesim1000fitresults, eval = FALSE, fig.width=7, fig.height=7>>=
load(file = "simresults/grace_predictions.rdata")
ggplot(psdata,
  aes(argvals, estimate, group=S))+
  xlim(-5, 5) +
  geom_line(alpha = 0.01) +
  geom_line(aes(argvals + gamma0, ghat, group = Curve, colour = Curve, linetype = Curve), 
    data = dd2, size = 1.5) +
  facet_wrap(~ outcome) +
  opts(legend.position = "none") +
  ylab("Y") + xlab(expression(t + hat(gamma))) +
  opts(title = "D. Fitted curves from 1000 simulations")
ggsave("figure/gracesim1000fitresults.pdf", width = 6, height = 3)
@


<<truevsobservedshifts, fig.height=6>>=
load(file = "data/grace_simulation_fits.rdata")
dd1 <- do.call(rbind, lapply(grace.simulation.fits$fits, function(x) x$subs))
dd1$Outcome <-  factor(dd1$outcome, levels = paste("Outcome", 1:3))
dd1 <- subset(dd1, !duplicated(id))
dd2 <- merge(dd1[, c('id', 'gamma0')], subset(dd, !duplicated(id), c('id', 'gamma0')),
  by = 'id', suffixes = c('.est', '.true'))
dd3 <- with(dd2, rbind(
  cbind(gamma0.true = gamma0.true, gamma0.est = gamma0.true, Line = "Identity"),
  cbind(gamma0.true = gamma0.true, gamma0.est = predict(lm(gamma0.est~gamma0.true)), Line = "Regression")))
dd3 <- as.data.frame(dd3)
dd3 <- dd3[order(dd3$gamma0.true), ]
for(i in 1:2) dd3[,i] <- as.numeric(dd3[, i])
rownames(dd3) <- 1:nrow(dd3)
ggplot(dd2, aes(gamma0.true, gamma0.est)) +
 geom_point() +
 geom_line(aes(gamma0.true, gamma0.est, color = Line, linetype = Line), data = dd3) +
 opts(legend.position=c(0.85, 0.1)) +
 xlab(expression(gamma[0]))+
 ylab(expression(hat(gamma)))+
 opts(title = "Estimated versus true time shifts")
@


<<allfitted>>=
pdata <- predict(grace.simulation.fits, new.argvals.gammas = seq(-4, 4, by = 0.1))
pdata$Outcome <- factor(pdata$outcome, levels = paste("Outcome", 1:3))
pdata$Y <- pdata$estimate
pdata$t <- pdata$argvals
pdata <- pdata[order(pdata$t), ]
# pdata$lower <- with(pdata, ifelse(lower<0, 0, lower))
# pdata$upper <- with(pdata, ifelse(upper>100, 100, upper))

ggplot(pdata,
  aes(t, Y, group = Outcome)) +
  geom_smooth(aes(ymax = upper, ymin = lower, colour = Outcome), stat = 'identity') +
  facet_wrap(~ Outcome) +
  opts(legend.position = "none", title = "All subjects")
@

\begin{center}
\animategraphics[width=\linewidth,controls,loop,autoplay]{2}{iterationfigs/iteration_}{1}{10}
\end{center}

\newpage

<<DataPrep2>>=
set.seed(10)
x <- seq(-6, 6, by = 0.1)
f1 <- function(t){ 1*t }
f2 <- function(t){ 2*t }
f3 <- function(t){ -t }

pdd <- rbind(
  data.frame(x = x, y = f1(x), curve = "f1(x)"),
  data.frame(x = x, y = f2(x), curve = "f2(x)"),
  data.frame(x = x, y = f3(x), curve = "f3(x)"))  

qplot(x, y, data = pdd, colour = curve, linetype = curve, geom = "line")

t <- seq(-1, 1, by = 0.5)
n <- 100
id <- 1:n
gamma0 <- runif(n, min=-5, max=5)

dd <- cbind(id, gamma0)
dd <- merge(dd, data.frame(t))
sig <- 0.4
rho <- 0.1
dd <- data.frame(rbind(
  cbind(dd, Outcome = 1, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2))),
  cbind(dd, Outcome = 2, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2))),
  cbind(dd, Outcome = 3, rmvnorm(n, c(0,0), matrix(c(sig, rho, rho, sig), nrow = 2)))))
colnames(dd) <- c("id", "gamma0", "t", "Outcome", "alpha0", "alpha1")
dd$e <- rnorm(nrow(dd), sd = 0.8)

dd$Y0 <- with(dd, 
  ifelse(Outcome == 1, f1(t + gamma0),
  ifelse(Outcome == 2, f2(t + gamma0),
  f3(t + gamma0))))
dd$Y <- with(dd, Y0 + alpha0 + alpha1 * t + e)
dd <- dd[with(dd, order(id, Outcome, t)), ]
dd$Outcome <- paste("Outcome", dd$Outcome)
@

<<trueshifts2>>=
ggplot(dd, aes(t + gamma0, Y, group=id)) +
  geom_line(alpha = 1/8) +
  geom_line(aes(t + gamma0, Y0, group=NULL), size = 1.5) +
  facet_wrap(~ Outcome) +
  xlab(expression(t + gamma[0])) +
  xlim(-5, 5) +
  opts(title = "A. Simulated data with true curves and time shifts")
@

<<observed2>>=
ggplot(dd, aes(t, Y, group=id)) +
  geom_line(alpha = 1/8) +
  geom_point(size = 1, alpha = 1/8) +
  facet_wrap(~ Outcome) +
  xlim(-5, 5) +
  opts(title = "B. Simulated data as observed without time shifts")
@

<<part_residuals_g2>>=
ggplot(dd, aes(t+gamma0, Y - alpha0 - alpha1*t, group=id)) +
  geom_line(alpha = 1/8) +
  geom_point(size = 1, alpha = 1/8) +
  facet_wrap(~ Outcome) +
  xlim(-5, 5) +
  xlab(expression(t+gamma[0])) +
  ylab(expression(R^g * (t)*"="*Y[ij](t) - alpha[0*ij] - alpha[1*ij]*t)) +
  opts(title = "Partial residuals for estimating g")
@

<<part_residuals_alpha2>>=
ggplot(dd, aes(t, Y - Y0, group=id)) +
  geom_line(alpha = 1/8) +
  geom_point(size = 1, alpha = 1/8) +
  facet_wrap(~ Outcome) +
  xlim(-5, 5) +
  xlab(expression(t)) +
  ylab(expression(R^alpha * (t)*"="*Y[ij](t) - g(t+gamma[i]))) +
  opts(title = expression("Partial residuals for estimating "*alpha))
@


<<gracefits2, eval = FALSE>>=

# argvals <- dd$t; y <- dd$Y; outcome <- dd$Outcome; id <- dd$id
# group = NULL;
# tolerance = 1e-3; maxiter = 20;
# norder = 6; ngrid = 5; Lfdobj = 3; lambda = 10^(4);
# verbose = FALSE; plots = TRUE; figdir = file.path(getwd(), "iterationfigs")

grace.simulation.fits.2 <- with(dd, grace(t, Y, Outcome, id, plots = TRUE,
  figdir = file.path(getwd(), 
          "iterationfigs2")))

save(grace.simulation.fits.2, file = "data/grace_simulation_fits_2.rdata")
@

<<sigma2>>=
load("data/grace_simulation_fits_2.rdata")
ggplot(grace.simulation.fits.2$sigma, aes(Iteration, sigma, group=outcome))+
  geom_line(aes(colour = outcome)) +
  ylab(expression(sigma[epsilon]))
@

\newpage
\section{Results}
<<simallobserved2>>=
load(file = "data/grace_simulation_fits_2.rdata")
dd1 <- do.call(rbind, lapply(grace.simulation.fits.2$fits, function(x) x$subs))
dd1$Outcome <-  factor(dd1$outcome, levels = paste("Outcome", 1:3))
dd1.true <- dd1
dd1.true$ghat <- with(dd1, 
  ifelse(Outcome == "Outcome 1", f1(argvals + gamma0),
  ifelse(Outcome == "Outcome 2", f2(argvals + gamma0),
  f3(argvals + gamma0))))
dd1.true$y <- NA
dd1$Curve <- "Estimated"
dd1.true$Curve <- "True"
dd2 <- rbind(dd1, dd1.true)
dd2 <- dd2[!duplicated(with(dd2, paste(argvals + gamma0, Curve, Outcome))), ]
dd2 <- dd2[with(dd2, order(Curve, Outcome, argvals+gamma0)), ]
ggplot(dd1, aes(argvals + gamma0, y, group=id))+
  geom_line(alpha = 1/8) +
  geom_line(aes(argvals + gamma0, ghat, group = Curve, colour = Curve, linetype = Curve), 
    data = dd2, size = 1.5) +
  xlim(-5, 5) +
  facet_wrap(~ Outcome) +
  opts(legend.position=c(0.775, 0.25)) +
  ylab("Y") + xlab(expression(t + hat(gamma))) +
  opts(title = "C. Estimated and true curves with estimated time shifts")
@

<<truevsobservedshifts2, fig.height=6>>=
load(file = "data/grace_simulation_fits_2.rdata")
dd1 <- do.call(rbind, lapply(grace.simulation.fits.2$fits, function(x) x$subs))
dd1$Outcome <-  factor(dd1$outcome, levels = paste("Outcome", 1:3))
dd1 <- subset(dd1, !duplicated(id))
dd2 <- merge(dd1[, c('id', 'gamma0')], subset(dd, !duplicated(id), c('id', 'gamma0')),
  by = 'id', suffixes = c('.est', '.true'))
dd3 <- with(dd2, rbind(
  cbind(gamma0.true = gamma0.true, gamma0.est = gamma0.true, Line = "Identity"),
  cbind(gamma0.true = gamma0.true, gamma0.est = predict(lm(gamma0.est~gamma0.true)), Line = "Regression")))
dd3 <- as.data.frame(dd3)
dd3 <- dd3[order(dd3$gamma0.true), ]
for(i in 1:2) dd3[,i] <- as.numeric(dd3[, i])
rownames(dd3) <- 1:nrow(dd3)
ggplot(dd2, aes(gamma0.true, gamma0.est)) +
 geom_point() +
 geom_line(aes(gamma0.true, gamma0.est, color = Line, linetype = Line), data = dd3) +
 opts(legend.position=c(0.85, 0.1)) +
 xlab(expression(gamma[0]))+
 ylab(expression(hat(gamma)))+
 opts(title = "Estimated versus true time shifts")
@


<<allfitted2>>=
pdata <- predict(grace.simulation.fits.2, new.argvals.gammas = seq(-4, 4, by = 0.1))
pdata$Outcome <- factor(pdata$outcome, levels = paste("Outcome", 1:3))
pdata$Y <- pdata$estimate
pdata$t <- pdata$argvals
pdata <- pdata[order(pdata$t), ]
# pdata$lower <- with(pdata, ifelse(lower<0, 0, lower))
# pdata$upper <- with(pdata, ifelse(upper>100, 100, upper))

ggplot(pdata,
  aes(t, Y, group = Outcome)) +
  geom_smooth(aes(ymax = upper, ymin = lower, colour = Outcome), stat = 'identity') +
  facet_wrap(~ Outcome) +
  opts(legend.position = "none", title = "All subjects")
@

\begin{center}
\animategraphics[width=\linewidth,controls,loop,autoplay]{2}{iterationfigs2/iteration_}{1}{10}
\end{center}

\end{document}