\documentclass{tufte-handout}

%\geometry{showframe}% for debugging purposes -- displays the margins

\usepackage{amsmath}

% Set up the images/graphics package
\usepackage{graphicx}
\setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
\graphicspath{{graphics/}}

\title{Estimating long term progression\\ from short term data}
\author{
Michael Donohue, Helene Jacqmin-Gadda, M\'elanie Le Goff, Ronald Thomas,
Rema Raman, Anthony Gamst, Laurel Beckett, Clifford Jack, Michael Weiner,
Jean-Francois Dartigues, Paul Aisen}

\date{February 15, 2013}  % if the \date{} command is left out, the current date will be used

% The following package makes prettier tables.  We're all about the bling!
\usepackage{booktabs}

% The units package provides nice, non-stacked fractions and better spacing
% for units.
\usepackage{units}

% The fancyvrb package lets us customize the formatting of verbatim
% environments.  We use a slightly smaller font.
\usepackage{fancyvrb}
\fvset{fontsize=\normalsize}

% Small sections of multiple columns
\usepackage{multicol}

% Provides paragraphs of dummy text
\usepackage{lipsum}

% These commands are used to pretty-print LaTeX commands
\newcommand{\doccmd}[1]{\texttt{\textbackslash#1}}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newenvironment{docspec}{\begin{quote}\noindent}{\end{quote}}% command specification environment
\newcommand{\docenv}[1]{\textsf{#1}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}}% package name
\newcommand{\doccls}[1]{\texttt{#1}}% document class name
\newcommand{\docclsopt}[1]{\texttt{#1}}% document class option name

\newcommand{\Yij}{Y_{ij}}
\newcommand{\aaij}{\alpha_{0ij}}
\newcommand{\abij}{\alpha_{1ij}}
\newcommand{\gami}{\gamma_i}

\begin{document}

\maketitle% this prints the handout title, author, and date

\begin{marginfigure}% l b r t
  \includegraphics[width = 1.3\linewidth, trim = 0in 2in 0in 0in, clip]{../adni/figure/JackFigure2.png}
  \caption{Dynamic biomarkers of the Alzheimer's pathological cascade hypothesized by Jack et al 2010.}
  \label{fig:jack}
\end{marginfigure}

\begin{abstract}
\noindent \textbf{Motivation:} Diseases that progress slowly are often studied by observing cohorts at different stages of disease for short periods of time. A dynamic model of long term progression of Alzheimer's disease (AD) has been proposed by Jack, et al (2010) (Figure~\ref{fig:jack}). The AD Neuroimaging Initiative (ADNI) follows patient cohorts with various degrees of cognitive impairment, from normal to impaired (Figure~\ref{fig:adni}). The initiative includes a rich panel of novel cognitive tests, biomarkers, and brain images collected every six months for up to six years. The relative timing of the observations with respect to disease pathology is unknown. We apply an Alternating Conditional Expectation algorithm to estimate pathologic timing and long-term growth curves.\\
\noindent \textbf{Results:} Simulations demonstrate that the method can recover long-term disease trends from short-term observations and temporal ordering of individuals with respect to disease pathology, providing subject-specific prognostic estimates of the time until onset of symptoms. The growth curves estimated from ADNI data support prevailing theories of the Alzheimer's disease cascade. Other datasets with common outcome measures could be combined using the proposed algorithm.\\
\noindent \textbf{Availability:} Software to fit the proposed model and reproduce results with R is available from (\href{http://mdonohue.bitbucket.org/grace}{http://mdonohue.bitbucket.org/grace}). ADNI data can be downloaded from the Laboratory of NeuroImaging (\href{http://www.loni.ucla.edu}{http://www.loni.ucla.edu}).
\end{abstract}

\begin{marginfigure}% l b r t
%  \includegraphics[width = 1.3\linewidth, trim = 0in 2in 0in 0in, clip]{figure/JackFigure2.png}
  \includegraphics[width = 1.4\linewidth]{../adni/figure/ADNI_long_bl.pdf}
  \caption{The ADNI battery of biomarkers and cognitive and functional assessments plotted by time from first ADNI visit. Not all subject received the complete battery of imaging and biomarker studies. ADNI includes Cognitively Normal (CN), Early \& Late Mild Cognitive Impairment (EMCI \& LMCI), and probable Alzheimer's Disease (AD) cohorts. \textbf{\emph{There is no obvious biological ``time zero'', making estimation of the hypothetical curves in Figure 1 difficult.}}}
  \label{fig:adni}
\end{marginfigure}

%\printclassoptions

% \includegraphics[scale = 0.3]{}


\section{The proposed multivariate model}\label{sec:model}
\noindent For each of the outcomes $j = 1, \ldots, m$, individual $i = 1, \ldots, n$, and observation times $t$; we propose:
$$Y_{ij}(t) = g_j(t + \gami) + \alpha_{0ij} + \alpha_{1ij} t + \varepsilon_{ij}(t)$$
\begin{itemize}
  \item $g_j$ monotone (non-decreasing or non-increasing)
  \item subject-specific time shift $\gami$ is Gaussian with mean 0
  \item subject- and outcome- specific random effects $(\alpha_{0ij}, \alpha_{1ij})$ are bivariate Gaussian with mean 0 and covariance matrix $\Sigma_j$
  \item $\varepsilon_{ij}(t)$ are independent Gaussian residual errors with mean 0
\end{itemize}
%To simplify notation, we think of $t$ as both a covariate and a continuous valued index. 
The model is related to \emph{shape invariant models} (Lawton et al 1972), and \emph{generalized additive models} (Hastie \& Tibshirani, 1981).

\clearpage

\begin{figure}[!tpb]% l b r t , trim = 0in 5.22in 0in 0in, clip
%\includegraphics[width = 0.90\linewidth]{figure/ADNI_fits}
\centerline{\includegraphics[width=0.99\textwidth, trim = 0in 0.35in 0in 0.2in, clip]{../adni/figure/ADNI_amylp_80ptime.pdf}}
\centerline{\includegraphics[width=0.99\textwidth, trim = 0in 0.3in 0in 0.45in, clip]{../adni/figure/ADNI_super_fits_amylp.pdf}}
\centerline{\includegraphics[width=0.99\textwidth, trim = 0in 0in 0in 0.45in, clip]{../adni/figure/ADNI_super_stand_slopes_amylp.pdf}}
\caption{Model fitted to ADNI subjects with abnormal amyloid detected in CSF or with PET scan. Time has shifted so that CDRSB trajectory (not shown) attains the 80$th$ percentile at time 0. The shaded regions in the top panel represent 95\% confidence bands. The middle panel superimposes the trajectories, and the bottom panel depicts standardized slopes over time.}
\label{fig:adnifits}
\end{figure}

\marginnote[-5.0in]{\textbf{Define \emph{partial residuals}:}
\begin{itemize}
  \item $R^{g}_{ij}(t) = \Yij(t) - \aaij - \abij t$
  \item $R^{\alpha}_{ij}(t) = \Yij(t) - g_j(t + \gami)$
  \item $R^\gamma_{ij}(t) = g_j^{-1}(Y_{ij}(t)) - t$
%  \item $R^\gamma_{ij}(t) = t - g_j^{-1}(Y_{ij}(t) - \aaij - \abij t - \varepsilon_{ij}(t))$
\end{itemize}
If we assume the model is correct then:
\begin{itemize}
  \item $E(R^{g}_{ij}(t)|g_j, t, \gami) = g_j(t + \gami)$
  \item $E(R^{\alpha}_{ij}(t)|\aaij, \abij, t) = \aaij + \abij t$
  \item $E(R^\gamma_{ij}(t)|\gami) \approx g_j^{-1}(g_j(t + \gami)) - t = \gami$\\
  (for small $\alpha_0,\alpha_1,\varepsilon$)
%  \item $g(t-R^\gamma_{ij}(t)) = t - g_j^{-1}(g_j(t + \gami)) = \gami$
\end{itemize}
}% end marginnote

\marginnote[-3in]{\textbf{Algorithm:}\\
Initialize $\gami=0$ and iterate:
\begin{enumerate}
  \item Given $\gami$, estimate the monotone functions $g_j$ by setting $\aaij = \abij = 0$ and iterating the following subroutine.
  \begin{itemize}
    \item[a.] Estimate $g_j$ by a monotone smooth of $R^{g}_{ij}(t)$.
    \item[b.] Estimate $\aaij, \abij$ by linear mixed-model of $R^{\alpha}_{ij}(t)$. Repeat Steps a and b until convergence of \\
    RSS$_j$ = $\sum_{it} (\Yij(t) - g_j(t + \gami))^2$.
  \end{itemize}
  \item Given current set of $g_j$, estimate each $\gami$ with the average of $R^\gamma_{ij}(t)$ over all $j$ and $t$. Repeat until convergence of \\
  RSS = $\sum_{ijt} (\Yij(t) - g_j(t + \gami))^2$.
\end{enumerate}
}%end margin note


\section{References:}
\begin{fullwidth}
{\tiny
\begin{itemize}
\item Jack, J.R., Knopman, D.S., Jagust, W.J., Shaw, L.M., Aisen, P.S., Weiner, M.W., Petersen, R.C., Trojanowski, J.Q. (2010). Hypothetical model of dynamic biomarkers of the Alzheimer's pathological cascade. {\it Lancet Neurol.} {\bf 9, 1}, 119.

\item Lawton W.H., Sylvestre E.A., Maggio M.S. Self modeling nonlinear regression. \emph{Technometrics} 1972; 14(3):513-532.

\item Hastie,T., \& Tibshirani, R. (1986). Generalized Additive Models. {\it Statistical Science}, {\bf 1, 3}, 297-310.

\end{itemize}
}
\end{fullwidth}

\end{document}
