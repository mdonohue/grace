# grace: Growth Models via Alternating Conditional Expectations

Diseases that progress over long periods of time are often studied by observing cohorts at different stages of disease for shorter periods of time. We apply an Alternating Conditional Expectation algorithm to estimate long-term multivariate monotone growth curves from short-term observations with unknown relative observation times.

## To install, from an R prompt:

```r
install_packages("devtools")
library(devtools)
install_bitbucket("grace", "mdonohue")
```
