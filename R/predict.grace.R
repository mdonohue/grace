predict.grace <-
function(fit, new.argvals.gammas, level = 0.95, verbose = FALSE, se = "loess"){
  require(fda)
  do.call(rbind, lapply(fit$fits, function(x){
      if(verbose) print(as.character(x$subset[1, "outcome"]))
      
      CI <- confint.grace(x, new.argvals.gammas, level = level, se = se)
    
      data.frame(outcome = x$subset[1, "outcome"],
        argvals = CI$new.argvals,
        estimate = CI$estimate, 
        lower = CI$lower,
        upper = CI$upper)
  }))
}

confint.grace <- 
function(x, new.argvals = sort(x$monotone$argvals), level = 0.95, se = "loess")
{
  result <- x$monotone
  res  <- residuals(result)
  Var <- var(res)

  if(class(x$lme) == "mer"){
    Designmat <- model.matrix(lme4::terms(x$lme), 
      data.frame(argvals = rep(0, length(new.argvals)), smooth.resids = 0))
    predvar <- Matrix::diag(Designmat %*% lme4::vcov(x$lme) %*% t(Designmat))
    lme.var <- predvar[1]
  }else{
    lme.var <- 0
  }

  new.argvals <- new.argvals[
    new.argvals >= min(result$argval) &
    new.argvals <= max(result$argval)
  ]
  beta <- result$beta
  Wfd  <- result$Wfdobj
  estimate <- beta[1] + beta[2]*eval.monfd(new.argvals, Wfd)

  if(se == "monotone"){
    y2cMap <- result$y2cMap
    c2rMap <- eval.basis(new.argvals, Wfd$basis)
    SigmaE <- diag(as.vector(rep(Var, ncol(y2cMap))))
    Sigmayhat <- c2rMap %*% y2cMap %*% SigmaE %*%
               t(y2cMap) %*% t(c2rMap)
    stderr <- sqrt(diag(Sigmayhat) + lme.var)
  }else if(se == "loess"){
    fit.loess <- with(result, loess(y~argvals))
    pred.loess <- predict(fit.loess, new.argvals, se = TRUE)
    stderr <- sqrt(pred.loess$se.fit^2 + lme.var)
  }else stop("se must be 'loess' or 'monotone'.")

  crit <- abs(qnorm((1-level)/2))
  CI <- data.frame(new.argvals, estimate, stderr)
  CI$upper <- with(CI, estimate + crit*stderr)
  CI$lower <- with(CI, estimate - crit*stderr)
  # with(CI, plot(new.argvals, estimate, type = 'l'))
  # with(result, points(argvals, y))
  # with(CI, lines(new.argvals, upper, lty=2, lwd=2))
  # with(CI, lines(new.argvals, lower, lty=2, lwd=2))
  CI
}